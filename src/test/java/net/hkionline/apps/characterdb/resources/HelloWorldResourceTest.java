package net.hkionline.apps.characterdb.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.testing.junit.ResourceTestRule;
import net.hkionline.apps.characterdb.api.greeting.Hello;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import java.io.IOException;

/**
 * Created by hkesseli on 6.2.2017.
 */
public class HelloWorldResourceTest {

    private static final String template = "Hello, %s!";
    private static final String defaultName = "Test Name";

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new HelloWorldResource(template, defaultName))
            .build();

    @Test
    public void sayHelloTest() throws IOException {

        final String helloJson = resources.client().target("/hello-world").request().get(String.class);
        final Hello hello = new ObjectMapper().readValue(helloJson, Hello.class);

        final String expectedGreeting = String.format(template, defaultName);
        Assert.assertEquals(expectedGreeting, hello.getGreeting());
    }


}
