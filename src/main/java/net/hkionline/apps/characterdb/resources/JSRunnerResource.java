package net.hkionline.apps.characterdb.resources;

import com.codahale.metrics.annotation.Timed;
import net.hkionline.apps.characterdb.core.scripting.JSRunner;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by hkesseli on 10.11.2016.
 */


@Path("/run")
public class JSRunnerResource {

    @POST
    @Path("/script")
    @Timed
    @Consumes("application/javascript")
    @Produces("application/javascript")
    public Response runJS(String javascriptAsString){

        String response = new JSRunner().runScript(javascriptAsString);
        return Response.ok(response).build();
    }

}
