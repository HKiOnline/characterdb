package net.hkionline.apps.characterdb.resources;

import com.codahale.metrics.annotation.Timed;
import io.dropwizard.validation.Validated;
import net.hkionline.apps.characterdb.api.character.Character;
import net.hkionline.apps.characterdb.api.character.Id;
import net.hkionline.apps.characterdb.db.ideal.CharacterDB;
import net.hkionline.apps.characterdb.db.query.SearchObject;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by hkesseli on 26.10.2016.
 */

@Path("/characters")
public class CharacterResource {

    private CharacterDB db;

    public CharacterResource(CharacterDB characterDB){this.db = characterDB;}

    @POST
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Character createCharacter(@Validated Character character){
        return db.create(character);
    }

    @PUT
    @Path("/" + Id.PATH_PARAM)
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Character updateCharacter(@PathParam(Id.KEY) @Pattern(regexp = Id.PATTERN) String id, @Validated Character character){
        character.setId(id);
        return db.update(character);
    }


    @DELETE
    @Path("/" + Id.PATH_PARAM)
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Character deleteCharacter(@PathParam(Id.KEY) @Pattern(regexp = Id.PATTERN) String id){
        return db.delete(id);
    }

    @GET
    @Path("/" + Id.PATH_PARAM)
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public Character getCharacterById(@PathParam(Id.KEY) @Pattern(regexp = Id.PATTERN) String id){ return db.getById(id); }

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public List<Character> getAllCharacters(){
        return db.search(new SearchObject());
    }

}
