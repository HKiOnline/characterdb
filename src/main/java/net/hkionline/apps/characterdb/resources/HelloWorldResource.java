package net.hkionline.apps.characterdb.resources;

import com.codahale.metrics.annotation.Timed;
import net.hkionline.apps.characterdb.api.greeting.Hello;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Created by hkesseli on 19.10.2016.
 */

@Path("/hello-world")
@Produces(MediaType.APPLICATION_JSON)
public class HelloWorldResource {


    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    public HelloWorldResource(String template, String defaultName){
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public Hello sayHello(@QueryParam("name") String name){

        final String value = String.format(template, name == null ? defaultName: name);
        return new Hello(counter.incrementAndGet(), value);
    }

}
