package net.hkionline.apps.characterdb;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import net.hkionline.apps.characterdb.db.dao.CharacterMDB;
import net.hkionline.apps.characterdb.health.TemplateHealthCheck;
import net.hkionline.apps.characterdb.resources.CharacterResource;
import net.hkionline.apps.characterdb.resources.HelloWorldResource;
import net.hkionline.apps.characterdb.resources.JSRunnerResource;

public class characterdbApplication extends Application<characterdbConfiguration> {

    public static void main(final String[] args) throws Exception {
        new characterdbApplication().run(args);
    }

    @Override
    public String getName() {
        return "characterdb";
    }

    @Override
    public void initialize(final Bootstrap<characterdbConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final characterdbConfiguration configuration, final Environment environment) {

        final HelloWorldResource helloWorldResource = new HelloWorldResource(configuration.getTemplate(), configuration.getDefaultName());
        final CharacterResource characterResource = new CharacterResource(new CharacterMDB());
        final JSRunnerResource jsRunnerResource = new JSRunnerResource();

        final TemplateHealthCheck templateHealthCheck = new TemplateHealthCheck(configuration.getTemplate());
        environment.healthChecks().register("template", templateHealthCheck);


        environment.jersey().register(helloWorldResource);
        environment.jersey().register(characterResource);
        environment.jersey().register(jsRunnerResource);
    }

}
