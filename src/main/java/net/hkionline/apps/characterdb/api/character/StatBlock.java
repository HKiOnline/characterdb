package net.hkionline.apps.characterdb.api.character;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by hkesseli on 26.10.2016.
 */
public class StatBlock {

    private int agility = 0;
    private int brawn = 0;
    private int intellect = 0;
    private int cunning = 0;
    private int willpower = 0;
    private int presence = 0;
    private int force = 0;

    private int species_wound_mod = 0;
    private int species_strain_mod = 0;

    private int melee_defense = 0;
    private int ranged_defense = 0;

    private List<Skill> skills;

    @JsonProperty
    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    @JsonProperty
    public int getBrawn() {
        return brawn;
    }

    public void setBrawn(int brawn) {
        this.brawn = brawn;
    }

    @JsonProperty
    public int getIntellect() {
        return intellect;
    }

    public void setIntellect(int intellect) {
        this.intellect = intellect;
    }

    @JsonProperty
    public int getCunning() {
        return cunning;
    }

    public void setCunning(int cunning) {
        this.cunning = cunning;
    }

    @JsonProperty
    public int getWillpower() {
        return willpower;
    }

    public void setWillpower(int willpower) {
        this.willpower = willpower;
    }

    @JsonProperty
    public int getPresence() {
        return presence;
    }

    public void setPresence(int presence) {
        this.presence = presence;
    }


    @JsonProperty
    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    @JsonProperty
    public int getMelee_defense() {
        return melee_defense;
    }

    public void setMelee_defense(int melee_defense) {
        this.melee_defense = melee_defense;
    }

    @JsonProperty
    public int getRanged_defense() {
        return ranged_defense;
    }

    public void setRanged_defense(int ranged_defense) {
        this.ranged_defense = ranged_defense;
    }

    public void setSpeciesModifiers(int wound_mod, int strain_mod) {
        this.species_strain_mod = strain_mod;
        this.species_wound_mod = wound_mod;
    }

    @JsonProperty
    public int getSoak() {
        return this.brawn;
    }

    @JsonProperty("wound_threshold")
    public int getWoundThreshold() {
        return 10 + this.brawn;
    }

    @JsonProperty("strain_threshold")
    public int getStrainThreshold() {
        return 10 + this.willpower;
    }

    @JsonProperty
    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
