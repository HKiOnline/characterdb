package net.hkionline.apps.characterdb.api.character;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hkesseli on 26.10.2016.
 */
public class Skill {

    private String name;
    private CharacteristicType characteristic;
    private int rank;

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public CharacteristicType getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(CharacteristicType characteristic) {
        this.characteristic = characteristic;
    }

    @JsonProperty
    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
