package net.hkionline.apps.characterdb.api.character;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * Created by hkesseli on 26.10.2016.
 */
public class Character {

    @Pattern(regexp = Id.PATTERN)
    private String id;

    @NotEmpty
    private String name;

    private String career;
    private String[] specializations;

    @NotNull
    private StatBlock stats;

    public Character() {}

    public Character(String id) {
        this.id = id;
    }

    @JsonProperty
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    @JsonProperty
    public String[] getSpecializations() {
        return specializations;
    }

    public void setSpecializations(String[] specializations) {
        this.specializations = specializations;
    }

    @JsonProperty
    public StatBlock getStats() {
        return stats;
    }

    public void setStats(StatBlock stats) {
        this.stats = stats;
    }
}
