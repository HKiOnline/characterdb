package net.hkionline.apps.characterdb.api.greeting;


/**
 * Created by hkesseli on 6.2.2017.
 */
public interface Greeting {

    long getId();
    String getGreeting();

}
