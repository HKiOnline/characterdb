package net.hkionline.apps.characterdb.api.character;

import java.util.UUID;

/**
 * Created by hkesseli on 10.11.2016.
 */
public class Id {

    public final static String PATTERN = "[a-f0-9]{32}";
    public final static String KEY = "id";
    public final static String PATH_PARAM = "{id}";

    public static String generate(){
        return UUID.randomUUID().toString().replace("-", "").toLowerCase();
    }
}
