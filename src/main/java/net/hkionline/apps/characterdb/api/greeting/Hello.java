package net.hkionline.apps.characterdb.api.greeting;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

/**
 * Created by hkesseli on 19.10.2016.
 */
public class Hello implements Greeting {

    private long id;

    @Length(max = 3)
    private String greeting;

    public Hello() {
        // For Jackson
    }

    public Hello(long id, String content) {
        this.id = id;
        this.greeting = content;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getGreeting() {
        return greeting;
    }
}
