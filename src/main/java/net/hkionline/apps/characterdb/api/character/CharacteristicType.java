package net.hkionline.apps.characterdb.api.character;

/**
 * Created by hkesseli on 26.10.2016.
 */
public enum CharacteristicType {
    AGILITY("agility"),
    BRAWN("brawn"),
    INTELLECT("intellect"),
    CUNNING("cunning"),
    WILLPOWER("willpower"),
    PRESENCE("presence");

    String name;

    CharacteristicType(String name){
        this.name = name;
    }
}
