package net.hkionline.apps.characterdb.core.scripting;

import jdk.nashorn.api.scripting.NashornScriptEngineFactory;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import java.io.StringWriter;

/**
 * Created by hkesseli on 10.11.2016.
 */
public class JSRunner {

    public String runScript(String scriptAsJavaString){

        NashornScriptEngineFactory factory = new NashornScriptEngineFactory();
        ScriptEngine engine = factory.getScriptEngine(getEngineArguments());

        engine = setBindings(engine);

        StringWriter stringWriter = new StringWriter();
        engine.getContext().setWriter(stringWriter);

        try {
            engine.eval( scriptAsJavaString );
            return stringWriter.toString();
        } catch ( Exception e ){
            System.out.println("Running script failed. Reason: "+e.getMessage());
        }

        return null;
    }

    private String[] getEngineArguments(){
        return new String[] {
                "-strict",
                "--no-java",
                "--no-syntax-extensions"
        };
    }

    private ScriptEngine setBindings(ScriptEngine engine){

        Bindings bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);

        bindings.put("API_VERSION", "1.0");
        bindings.put("Character", new JSCharacter());

        return engine;
    }

}
