package net.hkionline.apps.characterdb.core.scripting;

import jdk.nashorn.api.scripting.AbstractJSObject;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import jdk.nashorn.api.scripting.ScriptUtils;
import net.hkionline.apps.characterdb.api.character.Character;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by hkesseli on 10.11.2016.
 */
public class JSCharacter extends AbstractJSObject {

    private Character character;


    @Override
    public JSCharacter newObject(Object... args) {
        character = new Character();
        return this;
    }

    @Override
    public boolean isFunction() {
        return true;
    }

    @Override
    public Object getMember(String name) {
        Method method;

        String methodName = "get" + java.lang.Character.toUpperCase(name.charAt(0)) + name.substring(1);

        try {
            method = character.getClass().getMethod(methodName);
            return method.invoke(character);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void setMember(String name, Object value) {

        Method method;

        String methodName = "set" + java.lang.Character.toUpperCase(name.charAt(0)) + name.substring(1);

        try {
            if (value.getClass().equals(ScriptObjectMirror.class)){
                method = character.getClass().getMethod(methodName, String[].class);
                method.invoke(character, ScriptUtils.convert(value, String[].class));
            } else {
                method = character.getClass().getMethod(methodName, value.getClass());
                method.invoke(character, value);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }


}
