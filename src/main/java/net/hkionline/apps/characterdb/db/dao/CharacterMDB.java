package net.hkionline.apps.characterdb.db.dao;

import net.hkionline.apps.characterdb.api.character.Character;
import net.hkionline.apps.characterdb.api.character.Id;
import net.hkionline.apps.characterdb.db.ideal.CharacterDB;
import net.hkionline.apps.characterdb.db.query.SearchObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hkesseli on 28.10.2016.
 */
public class CharacterMDB implements CharacterDB {


    private Map<String, Character> characterCollection;

    public CharacterMDB(){
        characterCollection = new HashMap<>();
    }


    public Character create(Character character){

        if(character.getId() == null || character.getId().isEmpty()){
            character.setId(Id.generate());
        }

        characterCollection.put( character.getId(), character);
        return character;
    }

    public Character update(Character character){
        characterCollection.put( character.getId(), character);
        return character;
    }

    public Character delete(String id){
        Character character = characterCollection.get(id);
        characterCollection.remove(id);
        return character;
    }

    public List<Character> search(SearchObject searchObject) {
        ArrayList<Character> characters = new ArrayList<>();
        characters.addAll(characterCollection.values());
        return characters;
    }

    public Character getById(String id){
        return characterCollection.get(id);
    }


}
