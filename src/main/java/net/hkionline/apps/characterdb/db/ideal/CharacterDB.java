package net.hkionline.apps.characterdb.db.ideal;

import net.hkionline.apps.characterdb.api.character.Character;
import net.hkionline.apps.characterdb.db.query.SearchObject;

import java.util.List;

/**
 * Created by hkesseli on 28.10.2016.
 */
public interface CharacterDB {
    Character create(Character character);
    Character update(Character character);
    Character delete(String id);
    List<Character> search(SearchObject searchObject);
    Character getById(String id);
}
